var obj = {
    2:[
        '1号窟东壁右侧造像背光外圈共9身飞天乐伎',
        '1号窟东壁右侧造像背光内圈共7身飞天舞伎',
        '1号窟东壁右侧造像背光外圈琵琶乐伎',
        '1号窟东壁右侧造像背光外圈细腰鼓乐伎',
		'1号窟东壁右侧造像背光外圈横笛乐伎',
		'1号窟东壁右侧造像背光外圈热瓦普乐伎',
    ],
    3:[
        '藻井周围排箫乐伎',
        '藻井周围琵琶乐伎',
        '藻井周围长笛乐伎',
        '藻井周围细腰鼓乐伎',
        '藻井周围粗腰鼓乐伎',
        '藻井周围法螺乐伎',
        '藻井周围筚篥乐伎',
        '藻井周围箜篌乐伎',
    ],
    4:[
        '藻井2身',
        '藻井周围乐伎飞天褒衣博带左',
        '藻井周围乐伎飞天褒衣博带右',
    ],
    5:[
        '慧成龛背光细腰鼓乐伎',
        '慧成龛背光铜拨乐伎',
        '慧成龛背光细腰鼓乐伎',
        '慧成龛背光排箫乐伎',
        '慧成龛背光横笛乐伎',
        '慧成龛背光箫乐伎',
        '慧成龛背光筚篥乐伎',
        '慧成龛背光法螺乐伎',
        '南壁龛背光细腰鼓乐伎',
        '南壁龛背光排箫乐伎',
        '南壁龛背光横笛乐伎',
        '北壁龛背光筝乐伎',
        '北壁龛背光横笛乐伎',
        '北壁龛背光阮乐伎',
        '北壁龛背光排箫乐伎',
        '北壁龛背光笙乐伎',
    ],
    6:[
        '主佛背光琵琶乐伎',
        '主佛背光笙乐伎',
        '主佛背光排箫乐伎',
        '主佛背光横笛乐伎',
    ],
    14:[
        '藻井周围6身飞天',
    ],
    16:[
        '金塔寺东窟中心塔柱窟伎乐天',
    ],
    17:[
        '琵琶天宫乐伎',
        '排箫天宫乐伎',
        '横笛天宫乐伎',
        '五弦天宫乐伎',
    ],
    18:[
        '248窟内南北两壁上绘天宫伎乐',
        '251窟北壁东侧佛说法图顶部8身',
        '251窟南壁东侧佛说法图顶部8身',
        '251窟南壁上部10身',
        '254窟四壁最上层绕窟一周天宫伎乐',
        '254窟四壁最下层绕窟一周药叉伎乐',
        '254窟中心柱四面壁的下部一周药叉乐伎',
        '257窟上端一排天宫乐伎',
        '257窟中心柱正面龛内南壁伎乐飞天',
        '257窟中心柱正面龛内北壁伎乐飞天',
        '259窟上层天宫伎乐',
    ],
    19:[
        '165窟佛传故事画琵琶乐伎',
        '165窟佛传故事画阮咸乐伎',
        '135窟浮雕排箫乐伎',
        '135窟浮雕长笛乐伎',
        '135窟浮雕铃钹乐伎',
    ]
}


	


	
		

var json = {
	"nodes": [
        
        

        //////////////////////////////////////////////////////////////////////////////////////////
    {
		"color": "#c71969",
		"name": "平洛道",
		"attributes": {},
		"id": "1",
		"symbolSize": 50
	}, {
		"color": "#c71969",
		"name": "武乡良侯店石窟",
		"attributes": {},
		"id": "2",
		"symbolSize": 32
	}, {
		"color": "#c71969",
		"name": "高平高庙山石窟",
		"attributes": {},
		"id": "3",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "左权石佛寺石窟",
		"attributes": {},
		"id": "4",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "龙门石窟古阳洞",
		"attributes": {},
		"id": "5",
		"symbolSize": 32
	}, {
		"color": "#c71969",
		"name": "龙门石窟弥勒龛",
		"attributes": {},
		"id": "6",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "高平羊头山石窟",
		"attributes": {},
		"id": "7",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "晋中祁县子洪石窟",
		"attributes": {},
		
		
		"id": "8",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "榆社圆子山石窟",
		"attributes": {},
		
		
		"id": "9",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "榆社响堂寺石窟",
		"attributes": {},
		
		
		"id": "10",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "高欢云洞石窟",
		"attributes": {},
		
		
		"id": "11",
		"symbolSize": 16
	}, {
		"color": "#c71969",
		"name": "平定开河寺石窟",
		"attributes": {},
		
		
		"id": "12",
		"symbolSize": 16
	},
    

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    {
		"color": "#1984c7",
		"name": "并邺道",
		"attributes": {},
		
		
		"id": "13",
		"symbolSize": 50
	}, {
		"color": "#1984c7",
		"name": "河北宣化下花园石窟",
		"attributes": {},
		
		
		"id": "14",
		"symbolSize": 16
	}, 
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    {
		"color": "#c76919",
		"name": "河西走廊",
		"attributes": {},
		
		
		"id": "15",
		"symbolSize": 50
	}, {
		"color": "#c76919",
		"name": "张掖马蹄寺石窟群",
		"attributes": {},
		
		
		"id": "16",
		"symbolSize": 19
	}, {
		"color": "#c76919",
		"name": "文殊山-前山-千佛洞",
		"attributes": {},
		
		
		"id": "17",
		"symbolSize": 16
	}, {
		"color": "#c76919",
		"name": "敦煌共11窟",
		"attributes": {},
		
		
		"id": "18",
		"symbolSize": 30
	}, {
		"color": "#c76919",
		"name": "庆阳北石窟寺",
		"attributes": {},
		
		
		"id": "19",
		"symbolSize": 16
	}, {
		"color": "#c76919",
		"name": "武威天梯山诸窟",
		"attributes": {},
		
		
		"id": "20",
		"symbolSize": 16
	}, {
		"color": "#c76919",
		"name": "千佛洞和后山千佛洞",
		"attributes": {},
		
		
		"id": "21",
		"symbolSize": 16
	}, {
		"color": "#c76919",
		"name": "古佛洞壁画",
		"attributes": {},
		
		
		"id": "22",
		"symbolSize": 16
	}, {
		"color": "#c76919",
		"name": "泾川王母宫石窟",
		"attributes": {},
		
		
		"id": "23",
		"symbolSize": 16
	}, {
		"color": "#c76919",
		"name": "甘肃合水保全寺",
		"attributes": {},
		
		
		"id": "24",
		"symbolSize": 16
	},


    
////////////////////////////////////////////////////////////////////////////////////////////
    {
        "color": "#441199",
        "name": "太原辽宁道",
        "attributes": {},
        
        
        "id": "25",
        "symbolSize": 50
    }, {
        "color": "#441199",
        "name": "义县万佛堂",
        "attributes": {},
        
        
        "id": "26",
        "symbolSize": 16
    }],



	"edges": [{
		"sourceID": "1",
		"attributes": {},
		"targetID": "2",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "3",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "4",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "5",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "6",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "7",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "8",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "9",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "10",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "11",
		"symbolSize": 1
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "12",
		"symbolSize": 1
	}, {
		"sourceID": "13",
		"attributes": {},
		"targetID": "14",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "16",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "17",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "18",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "19",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "20",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "21",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "22",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "23",
		"symbolSize": 1
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "24",
		"symbolSize": 1
	}, {
		"sourceID": "25",
		"attributes": {},
		"targetID": "26",
		"symbolSize": 1
	},  {
		"sourceID": "1",
		"attributes": {},
		"targetID": "13",
		
	}, {
		"sourceID": "1",
		"attributes": {},
		"targetID": "15",
		
	},  {
		"sourceID": "1",
		"attributes": {},
		"targetID": "25",
		
	}, {
		"sourceID": "13",
		"attributes": {},
		"targetID": "15",
		
	}, {
		"sourceID": "13",
		"attributes": {},
		"targetID": "25",
		
	}, {
		"sourceID": "15",
		"attributes": {},
		"targetID": "25",
		
	},]
}

var xxx = {nodes:[]};
json.nodes.forEach(function(b,i){
    xxx.nodes.push(b)
    if(obj[b.id]){
        obj[b.id].forEach(function(bb,ii){
            xxx.nodes.push({
                "color": b.color,
                "name": bb,
                "attributes": {},
                "id": b.id+'.'+ii,
                "symbolSize": 5
            })
            // json.edges.push({
            //     "sourceID": b.id,
            //     "attributes": {},
            //     "targetID": b.id+'.'+ii,
            // })
        })
        
    }
})
json.nodes = xxx.nodes

json.nodes.forEach(function(b,i){
    if(i%13==0){
        json.nodes.forEach(function(bb,ii){
				json.edges.push({
					"sourceID": b.id,
					"attributes": {},
					"targetID": bb.id,
				})
            
        })
    }
    
})