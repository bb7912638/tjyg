(function (t) {
  function a(a) {
    for (
      var e, r, o = a[0], _ = a[1], l = a[2], c = 0, d = [];
      c < o.length;
      c++
    )
      (r = o[c]),
        Object.prototype.hasOwnProperty.call(n, r) && n[r] && d.push(n[r][0]),
        (n[r] = 0);
    for (e in _) Object.prototype.hasOwnProperty.call(_, e) && (t[e] = _[e]);
    v && v(a);
    while (d.length) d.shift()();
    return i.push.apply(i, l || []), s();
  }

  function s() {
    for (var t, a = 0; a < i.length; a++) {
      for (var s = i[a], e = !0, r = 1; r < s.length; r++) {
        var _ = s[r];
        0 !== n[_] && (e = !1);
      }
      e && (i.splice(a--, 1), (t = o((o.s = s[0]))));
    }
    return t;
  }
  var e = {},
    n = {
      app: 0,
    },
    i = [];

  function r(t) {
    return (
      o.p +
      'js/' +
      ({
        about: 'about',
      }[t] || t) +
      '.' +
      {
        about: 'd7d3d563',
      }[t] +
      '.js'
    );
  }

  function o(a) {
    if (e[a]) return e[a].exports;
    var s = (e[a] = {
      i: a,
      l: !1,
      exports: {},
    });
    return t[a].call(s.exports, s, s.exports, o), (s.l = !0), s.exports;
  }
  (o.e = function (t) {
    var a = [],
      s = n[t];
    if (0 !== s)
      if (s) a.push(s[2]);
      else {
        var e = new Promise(function (a, e) {
          s = n[t] = [a, e];
        });
        a.push((s[2] = e));
        var i,
          _ = document.createElement('script');
        (_.charset = 'utf-8'),
          (_.timeout = 120),
          o.nc && _.setAttribute('nonce', o.nc),
          (_.src = r(t));
        var l = new Error();
        i = function (a) {
          (_.onerror = _.onload = null), clearTimeout(c);
          var s = n[t];
          if (0 !== s) {
            if (s) {
              var e = a && ('load' === a.type ? 'missing' : a.type),
                i = a && a.target && a.target.src;
              (l.message =
                'Loading chunk ' + t + ' failed.\n(' + e + ': ' + i + ')'),
                (l.name = 'ChunkLoadError'),
                (l.type = e),
                (l.request = i),
                s[1](l);
            }
            n[t] = void 0;
          }
        };
        var c = setTimeout(function () {
          i({
            type: 'timeout',
            target: _,
          });
        }, 12e4);
        (_.onerror = _.onload = i), document.head.appendChild(_);
      }
    return Promise.all(a);
  }),
    (o.m = t),
    (o.c = e),
    (o.d = function (t, a, s) {
      o.o(t, a) ||
        Object.defineProperty(t, a, {
          enumerable: !0,
          get: s,
        });
    }),
    (o.r = function (t) {
      'undefined' !== typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(t, Symbol.toStringTag, {
          value: 'Module',
        }),
        Object.defineProperty(t, '__esModule', {
          value: !0,
        });
    }),
    (o.t = function (t, a) {
      if ((1 & a && (t = o(t)), 8 & a)) return t;
      if (4 & a && 'object' === typeof t && t && t.__esModule) return t;
      var s = Object.create(null);
      if (
        (o.r(s),
        Object.defineProperty(s, 'default', {
          enumerable: !0,
          value: t,
        }),
        2 & a && 'string' != typeof t)
      )
        for (var e in t)
          o.d(
            s,
            e,
            function (a) {
              return t[a];
            }.bind(null, e),
          );
      return s;
    }),
    (o.n = function (t) {
      var a =
        t && t.__esModule
          ? function () {
              return t['default'];
            }
          : function () {
              return t;
            };
      return o.d(a, 'a', a), a;
    }),
    (o.o = function (t, a) {
      return Object.prototype.hasOwnProperty.call(t, a);
    }),
    (o.p = ''),
    (o.oe = function (t) {
      throw (console.error(t), t);
    });
  var _ = (window['webpackJsonp'] = window['webpackJsonp'] || []),
    l = _.push.bind(_);
  (_.push = a), (_ = _.slice());
  for (var c = 0; c < _.length; c++) a(_[c]);
  var v = l;
  i.push([0, 'chunk-vendors']), s();
})({
  0: function (t, a, s) {
    t.exports = s('56d7');
  },
  '2a56': function (t, a, s) {},
  '56d7': function (t, a, s) {
    'use strict';
    s.r(a);
    s('e260'), s('e6cf'), s('cca6'), s('a79d');
    var e = s('2b0e'),
      n = function () {
        var t = this,
          a = t.$createElement,
          e = t._self._c || a;
        return e(
          'div',
          {
            staticClass: 'container is-fluid is-fullhd',
            style: t.cssVariables,
            attrs: {
              id: 'app',
            },
          },
          [
            e(
              'div',
              {
                staticClass: 'fonts',
                class: {
                  pause: t.isPause,
                },
              },
              [
                e(
                  'div',
                  {
                    staticClass: 'samplefont AppearGXSection',
                  },
                  [
                    t._m(0),
                    e(
                      'div',
                      {
                        staticClass: 'f_container',
                      },
                      [
                        e(
                          'p',
                          {
                            staticClass: 'vf f_AppearGX has-text-centered',
                            style: {
                              fontVariationSettings:
                                NaN + t.fvs_AppearGX__v_APER,
                            },
                          },
                          [t._v('始まる')],
                        ),
                      ],
                    ),
                  ],
                ),
                e(
                  'div',
                  {
                    staticClass: 'samplefont SodegoGXSection',
                  },
                  [
                    e(
                      'div',
                      {
                        staticClass: 'f_container',
                      },
                      [
                        e(
                          'p',
                          {
                            staticClass: 'vf f_SodegoGX has-text-centered',
                            style: {
                              fontVariationSettings:
                                NaN +
                                t.fvs_SodegoGX__v_wght +
                                ",'LENG'" +
                                t.fvs_SodegoGX__v_LENG,
                            },
                          },
                          [t._v('テキストです'), e('br'), t._v('フォントです')],
                        ),
                      ],
                    ),
                  ],
                ),
                e(
                  'div',
                  {
                    staticClass: 'samplefont MinGoGXSection',
                  },
                  [
                    e(
                      'div',
                      {
                        staticClass: 'f_container',
                      },
                      [
                        e(
                          'p',
                          {
                            staticClass: 'vf f_MinGoGX has-text-centered',
                            style: {
                              fontVariationSettings:
                                NaN + t.fvs_MinGoGX__v_CNGE,
                            },
                          },
                          [
                            t._v('選択できる'),
                            e('br'),
                            t._v('検索にも引っかかる'),
                            e('br'),
                            t._v('これフォント？'),
                            e('br'),
                            t._v('おもしろい！'),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                t._m(1),
                e(
                  'div',
                  {
                    staticClass: 'samplefont MarkGXSection',
                  },
                  [
                    e(
                      'div',
                      {
                        staticClass: 'f_container',
                      },
                      [
                        e(
                          'p',
                          {
                            staticClass: 'vf f_MarkGX has-text-centered',
                            style: {
                              fontVariationSettings: NaN + t.fvs_MarkGX__v_MARK,
                            },
                          },
                          [t._v('お正月')],
                        ),
                      ],
                    ),
                    e(
                      'div',
                      {
                        staticClass: 'f_slider',
                      },
                      [
                        e(
                          'b-field',
                          [
                            e('b-slider', {
                              attrs: {
                                rounded: '',
                                size: 'is-large',
                                min: 30,
                                max: 100,
                                tooltip: !1,
                              },
                              model: {
                                value: t.fvs_MarkGX__v_MARK,
                                callback: function (a) {
                                  t.fvs_MarkGX__v_MARK = a;
                                },
                                expression: 'fvs_MarkGX__v_MARK',
                              },
                            }),
                          ],
                          1,
                        ),
                      ],
                      1,
                    ),
                  ],
                ),
                t._m(2),
                e(
                  'div',
                  {
                    staticClass: 'samplefont NobigoGXSection',
                  },
                  [
                    e(
                      'div',
                      {
                        staticClass: 'f_container',
                      },
                      [
                        e(
                          'p',
                          {
                            staticClass: 'vf f_NobigoGX has-text-centered',
                            style: {
                              fontVariationSettings:
                                NaN +
                                t.fvs_NobigoGX__v_wght +
                                ",'LENG'" +
                                t.fvs_NobigoGX__v_LENG,
                            },
                          },
                          [t._v('制作受付中')],
                        ),
                      ],
                    ),
                  ],
                ),
                t._m(3),
                e(
                  'div',
                  {
                    staticClass: 'samplefont footer',
                  },
                  [
                    e(
                      'div',
                      {
                        staticClass: 'columns is-tablet',
                      },
                      [
                        e(
                          'div',
                          {
                            staticClass: 'column is-3 is-offset-9',
                          },
                          [
                            e(
                              'p',
                              {
                                staticClass: 'footer_qandalink',
                              },
                              [
                                e(
                                  'a',
                                  {
                                    on: {
                                      click: function (a) {
                                        t.isQandaModalActive = !0;
                                      },
                                    },
                                  },
                                  [t._v('Q&A')],
                                ),
                              ],
                            ),
                            t._m(4),
                            t._m(5),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            e(
              'b-modal',
              {
                attrs: {
                  active: t.isQandaModalActive,
                  'full-screen': '',
                  scroll: 'keep',
                },
                on: {
                  'update:active': function (a) {
                    t.isQandaModalActive = a;
                  },
                },
              },
              [
                e(
                  'div',
                  {
                    staticClass: 'card canda_container',
                  },
                  [
                    e(
                      'div',
                      {
                        staticClass: 'card-content',
                      },
                      [
                        e(
                          'div',
                          {
                            staticClass: 'content',
                          },
                          [
                            e(
                              'div',
                              {
                                staticClass: 'qanda_header',
                              },
                              [
                                e(
                                  'h2',
                                  {
                                    staticClass: 'qanda_header_title',
                                  },
                                  [
                                    t._v(''),
                                    e('br'),
                                    t._v('オーダーバリアブルフォント'),
                                    e('br'),
                                    t._v('Q&A'),
                                  ],
                                ),
                                e(
                                  'p',
                                  {
                                    staticClass: 'qanda_header_logo',
                                  },
                                  [
                                    e(
                                      'a',
                                      {
                                        attrs: {
                                          href: '//www.moji-sekkei.jp/',
                                          target: '_blank',
                                        },
                                      },
                                      [
                                        e('img', {
                                          attrs: {
                                            src: s('cf05'),
                                            alt: '',
                                          },
                                        }),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            e('p', [t._v('はじめに')]),
                            e('p', [
                              t._v(
                                'オーダーバリアブルフォントでは、個別にフォントを制作いたします。',
                              ),
                              e('br'),
                              t._v(
                                ' 発注者様の目的（あるブランドのプロモーションや、ある企業のプロモーションなど）に対して自由にご使用いただけます。',
                              ),
                              e('br'),
                              t._v(
                                ' 媒体（Web、映像、アプリ組み込み、など）の利用制限はございません。',
                              ),
                            ]),
                            e('hr'),
                            e('p', [
                              t._v(
                                'これまでの質問をまとめました、お問合せの前にご参照ください。',
                              ),
                            ]),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q1.')],
                                ),
                                e('dd', [t._v('例えばどこに使おう？')]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A1.')],
                                ),
                                e('dd', [
                                  t._v(
                                    'タイトルや見出し・本文中のアクセントなど、注目させたいところに最適です。 ロゴマークから社名・ブランド名、文章中の特定な文字が可変する、など',
                                  ),
                                ]),
                              ],
                            ),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q2.')],
                                ),
                                e('dd', [t._v('どうやって使うの？')]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A2.')],
                                ),
                                e('dd', [
                                  t._v(
                                    'フォントデータ（WOFF形式）で納品します。',
                                  ),
                                  e('br'),
                                  t._v(
                                    ' それをサーバーにアップして、CSSで指定するだけです。',
                                  ),
                                  e('br'),
                                  t._v(
                                    ' ループで動かすことはもちろん、スクロールやバーで動かすだけでなく端末に応じて太さを変えたり動きを変えることも可能です。',
                                  ),
                                  e('br'),
                                  t._v(
                                    ' スマホの傾きに応じて動きを制御することもできます。',
                                  ),
                                  e('br'),
                                  t._v(
                                    ' テキストのままなので検索にも引っかかります。',
                                  ),
                                  e('br'),
                                  t._v(' CSS記述例は'),
                                  e(
                                    'span',
                                    {
                                      staticClass: 'qanda_strong',
                                    },
                                    [t._v('A7')],
                                  ),
                                  t._v('をご覧ください。'),
                                ]),
                              ],
                            ),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q3.')],
                                ),
                                e('dd', [t._v('案件の進め方はどうなるの？')]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A3.')],
                                ),
                                e('dd', [
                                  t._v('以下のような流れになります。'),
                                  e('br'),
                                  t._v(' 1.打合せ'),
                                  e('br'),
                                  t._v(' 2.ご発注'),
                                  e('br'),
                                  t._v(
                                    ' 3.試作作成・文字のデザイン・動きの決定',
                                  ),
                                  e('br'),
                                  t._v(' 4.バリアブルフォント制作・動作確認'),
                                  e('br'),
                                  t._v(' 5.納品'),
                                ]),
                              ],
                            ),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q4.')],
                                ),
                                e('dd', [
                                  t._v('スケジュールと予算は、どれぐらい？'),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A4.')],
                                ),
                                e('dd', [
                                  t._v(
                                    '日程と金額は応相談、フォントデータは買い切りです。更新費はなく、ずっと使えます。',
                                  ),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-note',
                                  },
                                  [t._v('※例')],
                                ),
                                e('dd', [
                                  t._v(
                                    '本Webサイトのサンプル（弊社フォントを使用）の場合',
                                  ),
                                  e('br'),
                                  t._v(' スケジュール：1週間程度'),
                                  e(
                                    'span',
                                    {
                                      staticClass: 'qanda_small',
                                    },
                                    [
                                      t._v(
                                        '（バリアブルフォント制作3-4日、表示検証2-3日）',
                                      ),
                                    ],
                                  ),
                                  e('br'),
                                  t._v(
                                    ' 費用：120,000円-250,000円程度（目安です）',
                                  ),
                                  e('br'),
                                  t._v(
                                    'マーク及びイラスト・文字数、動きの程度によって変動します。',
                                  ),
                                ]),
                              ],
                            ),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q5.')],
                                ),
                                e('dd', [
                                  t._v(
                                    '何文字から作れるの？納品形式は？フォントのライセンスは？',
                                  ),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A5.')],
                                ),
                                e('dd', [
                                  t._v(
                                    '1文字から可能です。WOFF形式です。当方のフォントを使うので問題ありません。',
                                  ),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-note',
                                  },
                                  [t._v('※注')],
                                ),
                                e('dd', [
                                  t._v(
                                    'デザイン（マーク及びイラスト・タイプフェイスなど）の持ち込みは可能ですが、',
                                  ),
                                  e('br'),
                                  t._v(
                                    ' 権利関係をクリアしているものに限ります。',
                                  ),
                                ]),
                              ],
                            ),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q6.')],
                                ),
                                e('dd', [t._v('どんなブラウザで使えるの？')]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A6.')],
                                ),
                                e('dd', [
                                  t._v('以下検証結果です。 '),
                                  e(
                                    'table',
                                    {
                                      staticClass: 'table qanda_table-caniuse',
                                    },
                                    [
                                      e('thead', [
                                        e('tr', [
                                          e(
                                            'th',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_os',
                                            },
                                            [t._v('OS')],
                                          ),
                                          e(
                                            'th',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('ブラウザ')],
                                          ),
                                          e(
                                            'th',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('表示')],
                                          ),
                                        ]),
                                      ]),
                                      e('tbody', [
                                        e('tr', [
                                          e(
                                            'th',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_os',
                                              attrs: {
                                                rowspan: '3',
                                              },
                                            },
                                            [t._v('Windows 10')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Edge 44')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Chrome 79')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Firefox 72')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'th',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_os',
                                              attrs: {
                                                rowspan: '3',
                                              },
                                            },
                                            [t._v('macOS 10.15')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Safari 13')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Chrome 79')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Firefox 72')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'th',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_os',
                                              attrs: {
                                                rowspan: '2',
                                              },
                                            },
                                            [t._v('iOS 13')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Safari 13')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('WebView (Chrome)')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'th',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_os',
                                              attrs: {
                                                rowspan: '3',
                                              },
                                            },
                                            [t._v('Android')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Chrome 79')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [
                                              t._v('WebView'),
                                              e('br'),
                                              e(
                                                'span',
                                                {
                                                  staticClass: 'qanda_small',
                                                },
                                                [
                                                  t._v(
                                                    '(Android 9 搭載標準ブラウザ)',
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e('tr', [
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_browser',
                                            },
                                            [t._v('Firefox 68')],
                                          ),
                                          e(
                                            'td',
                                            {
                                              staticClass:
                                                'qanda_table-caniuse_enabled',
                                            },
                                            [t._v('○')],
                                          ),
                                        ]),
                                        e(
                                          'tr',
                                          {
                                            staticClass:
                                              'qanda_table-caniuse_col-windows10ie11',
                                          },
                                          [
                                            e(
                                              'th',
                                              {
                                                staticClass:
                                                  'qanda_table-caniuse_os',
                                                attrs: {
                                                  rowspan: '1',
                                                },
                                              },
                                              [t._v('Windows 10')],
                                            ),
                                            e(
                                              'td',
                                              {
                                                staticClass:
                                                  'qanda_table-caniuse_browser',
                                              },
                                              [t._v('Internet Explorer 11')],
                                            ),
                                            e(
                                              'td',
                                              {
                                                staticClass:
                                                  'qanda_table-caniuse_enabled',
                                              },
                                              [t._v('−')],
                                            ),
                                          ],
                                        ),
                                        e(
                                          'tr',
                                          {
                                            staticClass:
                                              'qanda_table-caniuse_col-windows10ie11note',
                                          },
                                          [
                                            e(
                                              'td',
                                              {
                                                attrs: {
                                                  colspan: '3',
                                                },
                                              },
                                              [
                                                e(
                                                  'span',
                                                  {
                                                    staticClass: 'qanda_small',
                                                  },
                                                  [
                                                    t._v(
                                                      'IE11はバリアブルフォント非対応のため可変はしませんがWebフォントとしての表示はされます。',
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ]),
                                    ],
                                  ),
                                  e('p', [
                                    t._v('【VF対応ブラウザ'),
                                    e(
                                      'span',
                                      {
                                        staticClass: 'qanda_info',
                                      },
                                      [t._v('※')],
                                    ),
                                    t._v('】'),
                                    e('br'),
                                    t._v(' Chrome for desktop 62以上'),
                                    e('br'),
                                    t._v(' Chrome for Android 62以上'),
                                    e('br'),
                                    t._v(' Android WebView 62以上'),
                                    e('br'),
                                    t._v(' macOS 10.13 以上のSafari 11以上'),
                                    e('br'),
                                    t._v(' macOS 10.13 以上のFirefox 62以上'),
                                    e('br'),
                                    t._v(' iOS Safari 11以上'),
                                    e('br'),
                                    t._v(' Microsoft Edge 17以上'),
                                  ]),
                                  e('hr', {
                                    staticClass: 'qanda_hr-small',
                                  }),
                                  e('p', [
                                    t._v(
                                      '【VF対応デスクトップアプリケーション',
                                    ),
                                    e(
                                      'span',
                                      {
                                        staticClass: 'qanda_info',
                                      },
                                      [t._v('※')],
                                    ),
                                    t._v('】'),
                                    e('br'),
                                    t._v(
                                      ' Adobe Illustrator CC2018 (22.0.0)以上',
                                    ),
                                    e('br'),
                                    t._v(
                                      ' Adobe Photoshop CC2018 (19.0.0)以上',
                                    ),
                                    e('br'),
                                    t._v(' Adobe InDesign CC2020 (15.0)以上'),
                                    e('br'),
                                    t._v(' Sketch 59以上'),
                                  ]),
                                  e('hr', {
                                    staticClass: 'qanda_hr-small',
                                  }),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-note',
                                  },
                                  [t._v('※注')],
                                ),
                                e('dd', [
                                  t._v(
                                    '各システム・アプリケーションのバリアブルフォント対応状況です。',
                                  ),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-note',
                                  },
                                  [t._v('※注')],
                                ),
                                e('dd', [
                                  t._v(
                                    '制作したフォントの検証は上記のうち検証が必要な環境毎に個別に検証いたします。',
                                  ),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-note',
                                  },
                                  [t._v('※注')],
                                ),
                                e('dd', [
                                  t._v(
                                    'その他アプリ組み込みでもご利用いただけます。詳しくはお問い合わせくださいませ。',
                                  ),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-note',
                                  },
                                  [t._v('※注')],
                                ),
                                e('dd', [
                                  t._v(
                                    'デスクトップアプリケーションでの使用は',
                                  ),
                                  e(
                                    'span',
                                    {
                                      staticClass: 'qanda_strong',
                                    },
                                    [t._v('別途契約')],
                                  ),
                                  t._v('となります。'),
                                  e('br'),
                                  t._v('（TrueType形式で納品）'),
                                ]),
                              ],
                            ),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q7.')],
                                ),
                                e('dd', [t._v('CSSだけで使えるの？')]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A7.')],
                                ),
                                e('dd', [
                                  t._v(
                                    'はい、利用できます。手順は以下の通りです。',
                                  ),
                                  e('br'),
                                  t._v(' 1. CSSの'),
                                  e('code', [t._v('@font-face')]),
                                  t._v('でWebフォントの定義を行う'),
                                  e('br'),
                                  t._v(' 2. CSSセレクターへ'),
                                  e('code', [t._v('font-family')]),
                                  t._v('と'),
                                  e('code', [t._v('font-variation')]),
                                  t._v('の指定を行う'),
                                  e('br'),
                                  t._v(' 3. HTMLを記述し描画を確認する '),
                                  e('hr', {
                                    staticClass: 'qanda_hr-small',
                                  }),
                                  e('p', [
                                    t._v(
                                      '可変を使ったアニメーションを行いたい場合は、font-variationの値が変移する',
                                    ),
                                    e('code', [t._v('@keyframes')]),
                                    t._v('を作ることで実現できます。'),
                                  ]),
                                  e('hr', {
                                    staticClass: 'qanda_hr-small',
                                  }),
                                ]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-note',
                                  },
                                  [t._v('※')],
                                ),
                                e(
                                  'dd',
                                  {
                                    staticClass: 'qanda_info',
                                  },
                                  [
                                    t._v(
                                      '@keyframesを用いた、アニメーションするバリアブルフォントCSSの利用例',
                                    ),
                                  ],
                                ),
                                e('pre', [
                                  t._v(
                                    "@font-face {\n  font-family: 'Appear'; // フォント名の指定\n  src: url('./assets/fonts/Appear.woff2') format('woff2'), url('./assets/fonts/Appear.woff') format('woff'); // フォントファイルの指定\n}\n\n@keyframes Appear {\n  // Appearという名前のアニメーションを定義\n  0% { font-variation-settings: 'APER' 30,'wegh' 30 }  // キーフレーム毎の軸名と軸値の指定 (0%, 50%, 100%をキーフレームとする例)\n  50% { font-variation-settings: 'APER' 100,'wegh' 100 }\n  100% { font-variation-settings: 'APER' 30,'wegh' 30 }\n}\n\n.f_APER {\n  font-family: 'Appear'; // @font-faceで指定したフォント名の指定\n  animation-name: Appear; // @keyframsesの定義の指定\n  animation-duration: 7.5s; // 0%から100％に至るまでの時間(s=秒)\n  animation-timing-function: ease-in-out; // アニメーションの加速度の指定(ゆっくり始まり中間が速くゆっくり終わる例)\n  animation-iteration-count: infinite; // アニメーションのループの回数(0%->100%を無限にループする例)\n}",
                                  ),
                                ]),
                              ],
                            ),
                            e(
                              'dl',
                              {
                                staticClass: 'qanda',
                              },
                              [
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-q',
                                  },
                                  [t._v('Q8.')],
                                ),
                                e('dd', [t._v('JavaScriptから制御できる？')]),
                                e(
                                  'dt',
                                  {
                                    staticClass: 'qanda-a',
                                  },
                                  [t._v('A8.')],
                                ),
                                e('dd', [
                                  t._v('はい、利用できます。'),
                                  e('br'),
                                  t._v(' バリアブルフォントは、CSSの'),
                                  e('code', [t._v('font-variation')]),
                                  t._v(
                                    'プロパティの値により形状が決まりますので、',
                                  ),
                                  e('code', [t._v('font-variation')]),
                                  t._v(
                                    'プロパティをJavaScriptで動的に取り扱うことで、JavaScriptを使って自由に文字のかたちを可変させることが可能です。',
                                  ),
                                ]),
                              ],
                            ),
                            e('hr'),
                            e('p', [
                              t._v(
                                'ご不明な点ありましたら、お気軽にご相談ください。',
                              ),
                              e('br'),
                              t._v(' '),
                              e(
                                'a',
                                {
                                  attrs: {
                                    href: 'mailto:kinutaff@moji-sekkei.jp',
                                  },
                                },
                                [t._v('kinutaff@moji-sekkei.jp')],
                              ),
                            ]),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
          1,
        );
      },
      i = [
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s(
            'p',
            {
              staticClass: 'product_title',
            },
            [
              s(
                'span',
                {
                  staticClass: 'product_title-small',
                },
                [t._v('')],
              ),
              s('br'),
              t._v('オーダーバリアブルフォント'),
            ],
          );
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s(
            'div',
            {
              staticClass: 'samplefont Copy2GXSection',
            },
            [
              s(
                'div',
                {
                  staticClass: 'f_container',
                },
                [
                  s(
                    'p',
                    {
                      staticClass: 'vf f_Copy2GX has-text-centered',
                    },
                    [t._v('私のおばあちゃん')],
                  ),
                ],
              ),
            ],
          );
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s(
            'div',
            {
              staticClass: 'samplefont CopyGXSection',
            },
            [
              s(
                'div',
                {
                  staticClass: 'f_container',
                },
                [
                  s(
                    'p',
                    {
                      staticClass: 'vf f_CopyGX has-text-centered',
                    },
                    [t._v('月が綺麗ですね。')],
                  ),
                ],
              ),
            ],
          );
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s(
            'div',
            {
              staticClass: 'samplefont MoveGXSection',
            },
            [
              s(
                'div',
                {
                  staticClass: 'f_container sample7',
                },
                [
                  s(
                    'p',
                    {
                      staticClass: 'vf f_MoveGX has-text-centered',
                    },
                    [t._v('砧')],
                  ),
                  s(
                    'p',
                    {
                      staticClass: 'vf f_iro25SLGX has-text-centered',
                    },
                    [t._v('キヌタフォント'), s('br'), t._v('バリアブル')],
                  ),
                ],
              ),
            ],
          );
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s(
            'p',
            {
              staticClass: 'footer_maillink',
            },
            [
              s(
                'a',
                {
                  attrs: {
                    href: 'mailto:kinutaff@moji-sekkei.jp',
                  },
                },
                [t._v('お問合せ')],
              ),
            ],
          );
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s(
            'p',
            {
              staticClass: 'footer_logo',
            },
            [
              s(
                'a',
                {
                  attrs: {
                    href: 'http://www.moji-sekkei.jp/',
                  },
                },
                [t._v('')],
              ),
            ],
          );
        },
      ],
      r = {
        name: 'app',
        components: {},
        data: function () {
          return {
            scrollY: 0,
            bodyHeight: 0,
            windowHeight: 0,
            scrollRate: 0,
            isPause: !1,
            fvs_rootState: 0,
            min: 59,
            sec: 59,
            timerOn: !1,
            timerObjSlow: null,
            timerObjFast: null,
            fvs_AppearGX: 0,
            fvs_AppearGX__v_APER: 30,
            fvs_MarkGX: 0,
            fvs_MarkGX__v_MARK: 30,
            fvs_MinGoGX: 0,
            fvs_MinGoGX__v_CNGE: 30,
            fvs_NobigoGX: 0,
            fvs_NobigoGX__v_wght: 30,
            fvs_NobigoGX__v_LENG: 100,
            fvs_SodegoGX: 0,
            fvs_SodegoGX__v_wght: 30,
            fvs_SodegoGX__v_LENG: 100,
            isQandaModalActive: !1,
          };
        },
        mounted: function () {
          window.addEventListener('scroll', this.handleScroll),
            window.addEventListener('resize', this.handleScroll),
            this.handleScroll(),
            this.start();
        },
        methods: {
          start: function () {
            var t = this,
              a = 0;
            (this.timerObjSlow = setInterval(function () {
              var s = 65 + 35 * Math.sin(0.01 * a * Math.PI),
                e = 150 + 50 * Math.sin(0.08 * a * Math.PI),
                n = 65 + 35 * Math.sin(0.03 * a * Math.PI),
                i = 150 + 50 * Math.sin(0.06 * a * Math.PI),
                r = 65 + 35 * Math.sin(0.05 * a * Math.PI);
              (t.fvs_AppearGX__v_APER = s),
                (t.fvs_SodegoGX__v_wght = n),
                (t.fvs_NobigoGX__v_wght = r),
                (t.fvs_SodegoGX__v_LENG = i),
                (t.fvs_NobigoGX__v_LENG = e),
                (t.fvs_MinGoGX__v_CNGE = s),
                a++;
            }, 60)),
              (this.timerOn = !0);
          },
          handleScroll: function () {
            (this.scrollY = window.scrollY),
              (this.bodyHeight = document.body.offsetHeight),
              (this.windowHeight = window.innerHeight),
              (this.scrollRate =
                (this.scrollY + this.windowHeight / 2) / this.bodyHeight),
              (this.fvs_MarkGX__v_MARK =
                (385 * this.scrollRate - 200) * (380 * this.scrollRate - 200) +
                30);
          },
          loopSleep: function (t, a, s) {
            var e = t,
              n = a,
              i = s,
              r = 0,
              o = function t() {
                var a = i(r);
                !1 !== a &&
                  ((r += 1),
                  !1 === e ? setTimeout(t, n) : r < e && setTimeout(t, n));
              };
            o();
          },
        },
        computed: {
          cssVariables: function () {
            return {
              '--fvs_AppearGX__v_APER': this.fvs_AppearGX__v_APER,
              '--fvs_MarkGX__v_MARK': this.fvs_MarkGX__v_MARK,
              '--fvs_MinGoGX__v_CNGE': this.fvs_MinGoGX__v_CNGE,
              '--fvs_NobigoGX__v_wght': this.fvs_NobigoGX__v_wght,
              '--fvs_NobigoGX__v_LENG': this.fvs_NobigoGX__v_LENG,
              '--fvs_SodegoGX__v_wght': this.fvs_SodegoGX__v_wght,
              '--fvs_SodegoGX__v_LENG': this.fvs_SodegoGX__v_LENG,
            };
          },
        },
      },
      o = r,
      _ = (s('5c0b'), s('2877')),
      l = Object(_['a'])(o, n, i, !1, null, null, null),
      c = l.exports,
      v = (s('d3b7'), s('8c4f')),
      d = function () {
        var t = this,
          a = t.$createElement,
          e = t._self._c || a;
        return e(
          'div',
          {
            staticClass: 'home',
          },
          [
            e('img', {
              attrs: {
                alt: 'Vue logo',
                src: s('cf05'),
              },
            }),
            e('HelloWorld', {
              attrs: {
                msg: 'Welcome to Your Vue.js App',
              },
            }),
          ],
          1,
        );
      },
      f = [],
      u = function () {
        var t = this,
          a = t.$createElement,
          s = t._self._c || a;
        return s(
          'div',
          {
            staticClass: 'hello',
          },
          [
            s('h1', [t._v(t._s(t.msg))]),
            t._m(0),
            s('h3', [t._v('Installed CLI Plugins')]),
            t._m(1),
            s('h3', [t._v('Essential Links')]),
            t._m(2),
            s('h3', [t._v('Ecosystem')]),
            t._m(3),
          ],
        );
      },
      p = [
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s('p', [
            t._v(
              ' For a guide and recipes on how to configure / customize this project,',
            ),
            s('br'),
            t._v(' check out the '),
            s(
              'a',
              {
                attrs: {
                  href: 'https://cli.vuejs.org',
                  target: '_blank',
                  rel: 'noopener',
                },
              },
              [t._v('vue-cli documentation')],
            ),
            t._v('. '),
          ]);
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s('ul', [
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-babel',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('babel')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-router',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('router')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-vuex',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('vuex')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-eslint',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('eslint')],
              ),
            ]),
          ]);
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s('ul', [
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://vuejs.org',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('Core Docs')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://forum.vuejs.org',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('Forum')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://chat.vuejs.org',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('Community Chat')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://twitter.com/vuejs',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('Twitter')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://news.vuejs.org',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('News')],
              ),
            ]),
          ]);
        },
        function () {
          var t = this,
            a = t.$createElement,
            s = t._self._c || a;
          return s('ul', [
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://router.vuejs.org',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('vue-router')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://vuex.vuejs.org',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('vuex')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://github.com/vuejs/vue-devtools#vue-devtools',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('vue-devtools')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://vue-loader.vuejs.org',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('vue-loader')],
              ),
            ]),
            s('li', [
              s(
                'a',
                {
                  attrs: {
                    href: 'https://github.com/vuejs/awesome-vue',
                    target: '_blank',
                    rel: 'noopener',
                  },
                },
                [t._v('awesome-vue')],
              ),
            ]),
          ]);
        },
      ],
      b = {
        name: 'HelloWorld',
        props: {
          msg: String,
        },
      },
      h = b,
      C = (s('6f12'), Object(_['a'])(h, u, p, !1, null, '5cc7f8f0', null)),
      m = C.exports,
      g = {
        name: 'home',
        components: {
          HelloWorld: m,
        },
      },
      q = g,
      w = Object(_['a'])(q, d, f, !1, null, null, null),
      G = w.exports;
    e['a'].use(v['a']);
    var S = [
        {
          path: '/',
          name: 'home',
          component: G,
        },
        {
          path: '/about',
          name: 'about',
          component: function () {
            return s.e('about').then(s.bind(null, 'f820'));
          },
        },
      ],
      k = new v['a']({
        mode: 'history',
        base: '',
        routes: S,
      }),
      X = k,
      A = s('2f62');
    e['a'].use(A['a']);
    var E = new A['a'].Store({
        state: {},
        mutations: {},
        actions: {},
        modules: {},
      }),
      M = s('289d');
    e['a'].use(M['a']),
      (e['a'].config.productionTip = !1),
      new e['a']({
        router: X,
        store: E,
        Buefy: M['a'],
        render: function (t) {
          return t(c);
        },
      }).$mount('#app');
  },
  '5c0b': function (t, a, s) {
    'use strict';
    var e = s('9c0c'),
      n = s.n(e);
    n.a;
  },
  '6f12': function (t, a, s) {
    'use strict';
    var e = s('2a56'),
      n = s.n(e);
    n.a;
  },
  '9c0c': function (t, a, s) {},
  cf05: function (t, a, s) {
    t.exports =
      s.p + window.url_template + '/assets/images/common/title--ja.svg';
  },
});
//# sourceMappingURL=app.ffca62f4.js.map
